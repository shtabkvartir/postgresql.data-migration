package main

import (
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"text/tabwriter"

	pkgFileStorage "github.com/dochq/postgresql.data-migration/internal/filestorage"
	pkgMigration "github.com/dochq/postgresql.data-migration/internal/migration"
	pkgSql "github.com/dochq/postgresql.data-migration/internal/sqldb"
)

// AppVersion - application version.
var AppVersion string = "unversioned"

func main() {

	var (
		migrationsDir       string
		migrationsTableName string
		dataSourceName      string
	)

	// Define our flags.
	//
	flag.StringVar(&migrationsDir, "migrations", "migrations", "Directory where the migration files are located")
	flag.StringVar(&migrationsTableName, "migrations-table", "public.x_schema_migrations", "Name of the migrations table")
	flag.StringVar(&dataSourceName, "dsn", "", "Database source name (mandatory)")
	help := flag.Bool("help", false, "Display usage")
	version := flag.Bool("version", false, "Print version & exit")

	flag.Usage = usageFor(os.Args[0] + " [flags]")
	flag.CommandLine.SetOutput(os.Stdout)
	flag.Parse()

	if *help {
		fmt.Fprintf(os.Stdout, "Usage:\n")
		flag.PrintDefaults()
		return
	}
	if *version {
		appVersion := AppVersion
		if appVersion == "" {
			appVersion = "unversioned"
		}
		fmt.Println(appVersion)
		return
	}
	if len(dataSourceName) == 0 {
		dataSourceName = os.Getenv("DSN")
	}

	// Check migration inputs.
	//
	if err := checkMigrationInputs(migrationsDir, migrationsTableName, dataSourceName); err != nil {
		flag.Usage()
		log.Fatalln(err)
	}

	// Open database connection.
	//
	conn, err := pkgSql.GetConnection("postgres", dataSourceName)
	if err != nil {
		log.Fatalln("Cannot establish connection to database", err)
	}

	// Remember to close the database connection.
	//
	defer func(c *sql.DB) {
		if c != nil {
			_ = c.Close()
		}
	}(conn)

	// Establish connection to database.
	//
	pkgSql.EstablishConnectionWithRetry(conn)

	// Build the layers of the service "onion" from the inside out.
	//
	migrationStorage := pkgFileStorage.NewMigrationStorage(migrationsDir)
	migrationRepo := pkgSql.NewMigrationRepository(conn, migrationsTableName)
	queryParser := pkgSql.NewQueryParser()
	migrationService := pkgMigration.NewMigrationService(migrationRepo, migrationStorage, queryParser)

	// Check if the table exist.
	//
	if err := migrationRepo.EnsureTableExist(migrationsTableName); err != nil {
		log.Fatalln(err)
	}

	// Run migrations.
	//
	log.Println("Migration started")
	applied, err := migrationService.Migrate()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Done", applied)
}

func usageFor(short string) func() {
	return func() {
		_, _ = fmt.Fprintf(os.Stderr, "USAGE\n")
		_, _ = fmt.Fprintf(os.Stderr, "  %s\n", short)
		_, _ = fmt.Fprintf(os.Stderr, "\n")
		_, _ = fmt.Fprintf(os.Stderr, "INFO\n")
		_, _ = fmt.Fprintf(os.Stderr, "  version:  %s\n", AppVersion)
		_, _ = fmt.Fprintf(os.Stderr, "\n")
		_, _ = fmt.Fprintf(os.Stderr, "FLAGS\n")
		w := tabwriter.NewWriter(os.Stderr, 0, 2, 2, ' ', 0)

		flag.VisitAll(func(f *flag.Flag) {
			_, _ = fmt.Fprintf(w, "\t-%s\t%s\t%s\n", f.Name, f.DefValue, f.Usage)
		})

		_ = w.Flush()
		_, _ = fmt.Fprintf(os.Stderr, "\n")
	}
}

func checkMigrationInputs(migrationsDir, migrationsTableName, dataSourceName string) error {
	if len(migrationsDir) == 0 {
		return errors.New("migrations source directory required")
	}
	if len(migrationsTableName) == 0 {
		return errors.New("migrations table name required")
	}
	if len(dataSourceName) == 0 {
		return errors.New("database source name required")
	}
	return nil
}
