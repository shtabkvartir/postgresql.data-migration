package domain

import (
	"strconv"
	"time"
)

// Migration consts.
const (
	MigrationFilePattern = `(.*\/)?((\d+).(\d+).(\d+).*\.sql)$`
)

// Version - version struct.
type Version struct {
	Major int
	Minor int
	Patch int
}

// String - returns a string representation.
func (ver Version) String() string {
	return strconv.Itoa(ver.Major) + "." + strconv.Itoa(ver.Minor) + "." + strconv.Itoa(ver.Patch)
}

// ID - returns a string representation of ID.
func (ver Version) ID() string {
	return strconv.Itoa(ver.Major) + "." + strconv.Itoa(ver.Minor) + "." + strconv.Itoa(ver.Patch)
}

// Migration - migration struct.
type Migration struct {
	Version  Version
	Name     string
	Content  []string
	Metadata Metadata
}

// String - returns a string representation.
func (mig Migration) String() string {
	return mig.Version.String() + ": " + mig.Name
}

// SetExecutionTime - sets the migration execution time.
func (mig *Migration) SetExecutionTime(start, end time.Time) {
	mig.Metadata.StartTime = start.Unix()
	mig.Metadata.ExecutionTime = end.Unix() - start.Unix()
}

// Metadata - migrations metadata.
type Metadata struct {
	StartTime     int64
	ExecutionTime int64
}

// QueryExecutor - query executor.
type QueryExecutor interface {
	// ExecuteQueries - execute migration queries.
	ExecuteQueries(queries []string) error
}

// MigrationRepository - migration repository interface.
type MigrationRepository interface {
	QueryExecutor

	// EnsureTableExist - checks if migration table exists and, if not, creates it.
	EnsureTableExist(migrationsTable string) error

	// IsMigrationExist - checks if the migration exist.
	IsMigrationExist(migration *Migration) (bool, error)

	// CreateMigrationRecord - creates migration record.
	CreateMigrationRecord(migration *Migration) error
}

// MigrationStorage - migration storage.
type MigrationStorage interface {

	// GetExecutableMigrations - returns executable migrations.
	GetExecutableMigrations() ([]*Migration, error)
}

// MigrationService - migration service.
type MigrationService interface {

	// RunMigrations - runs migrations.
	Migrate() (applied int, err error)
}
