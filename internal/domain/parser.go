package domain

// QueryParser - query parser.
type QueryParser interface {

	// ParseQueries - takes lines as input and returns queries.
	ParseQueries(lines []string) (queries []string, err error)
}
