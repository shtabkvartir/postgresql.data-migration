package filestorage

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/dochq/postgresql.data-migration/internal/domain"
)

type storage struct {
	migrationsDir string
	pattern       *regexp.Regexp
}

// NewMigrationStorage creates a service with necessary dependencies.
func NewMigrationStorage(migrationsDir string) domain.MigrationStorage {
	return &storage{
		migrationsDir: migrationsDir,
		pattern:       regexp.MustCompile(domain.MigrationFilePattern),
	}
}

func (s *storage) GetExecutableMigrations() ([]*domain.Migration, error) {
	var migrations []*domain.Migration

	err := filepath.Walk(s.migrationsDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("error walking filepath: %v", err)
		}
		if info != nil && info.IsDir() {
			return nil
		}
		match := s.pattern.FindStringSubmatch(path)
		if match == nil {
			return fmt.Errorf("file is ignored, naming pattern is wrong: %s", path)
		}
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		var contents []string
		for scanner.Scan() {
			contents = append(contents, scanner.Text())
		}

		name, err := getTitle(match, 2, path)
		if err != nil {
			return err
		}
		major, err := getVersion(match, 3, path)
		if err != nil {
			return err
		}
		minor, err := getVersion(match, 4, path)
		if err != nil {
			return err
		}
		patch, err := getVersion(match, 5, path)
		if err != nil {
			return err
		}
		migration := &domain.Migration{
			Version: domain.Version{
				Major: major,
				Minor: minor,
				Patch: patch,
			},
			Name:    name,
			Content: contents,
		}

		migrations = append(migrations, migration)
		return nil
	})

	return migrations, err
}

func getTitle(match []string, i int, filename string) (string, error) {
	if len(match) > i {
		return match[i], nil
	}
	return "", fmt.Errorf("incorrect file naming pattern: %s", filename)
}

func getVersion(match []string, i int, filename string) (int, error) {
	if len(match) > i {
		return strconv.Atoi(match[i])
	}
	return 0, fmt.Errorf("incorrect file versioning pattern: %s", filename)
}
