package sqldb

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/dochq/postgresql.data-migration/internal/domain"
	"github.com/dochq/postgresql.data-migration/internal/helpers"
)

type migrationRepo struct {
	conn            *sql.DB
	migrationsTable string
}

// NewMigrationRepository creates a new repository.
func NewMigrationRepository(conn *sql.DB, migrationsTable string) domain.MigrationRepository {
	return &migrationRepo{
		conn:            conn,
		migrationsTable: migrationsTable,
	}
}

func (r *migrationRepo) EnsureTableExist(migrationsTable string) error {

	// If not, create the empty migration table.
	//
	query := `CREATE TABLE IF NOT EXISTS ` + migrationsTable + ` (version varchar(10) not null primary key, name varchar(255) not null, 
	start_time int not null, execution_time int not null)`
	if _, err := r.conn.ExecContext(context.Background(), query); err != nil {
		return fmt.Errorf("database error %v, query %s", err, query)
	}
	return nil
}

func (r *migrationRepo) IsMigrationExist(migration *domain.Migration) (bool, error) {
	var migrationID string
	query := `SELECT version FROM ` + r.migrationsTable + ` WHERE version = $1 LIMIT 1;`
	err := RunReadQuery(r.conn, query, helpers.WrapValue(migration.Version.ID()), func(rows *sql.Rows) error {
		err := rows.Scan(
			&migrationID,
		)
		return err
	})
	if err != nil {
		return false, err
	}
	return len(strings.TrimSpace(migrationID)) > 0, nil
}

func (r *migrationRepo) CreateMigrationRecord(migration *domain.Migration) error {
	query := `INSERT INTO ` + r.migrationsTable + ` (version, name, start_time, execution_time) VALUES($1, $2, $3, $4)`
	_, err := RunWriteQuery(r.conn, query, migration.Version.ID(), migration.Name,
		migration.Metadata.StartTime, migration.Metadata.ExecutionTime)
	return err
}

func (r *migrationRepo) ExecuteQueries(queries []string) error {
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	// Begin transaction.
	//
	tx, err := r.conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return err
	}

	// The rollback will be ignored if the tx has been committed later in the function.
	//
	defer func() {
		_ = tx.Rollback()
	}()

	// Execute prepared statements.
	//
	for _, query := range queries {
		stmt, err := tx.Prepare(query)
		if err != nil {
			return err
		}
		defer stmt.Close() // Prepared statements take up server resources and should be closed after use.
		if _, err := stmt.Exec(); err != nil {
			return err
		}
	}

	// Commit transaction.
	//
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
