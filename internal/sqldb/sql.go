package sqldb

import (
	"context"
	"database/sql"
	"log"
	"time"

	"github.com/cenk/backoff"
	// justifying it
	_ "github.com/lib/pq"
)

// GetConnection - returns database connection.
func GetConnection(driverName, dataSourceName string) (*sql.DB, error) {

	// Open may just validate its arguments without creating a connection to the database.
	//
	sqlconn, err := sql.Open(driverName, dataSourceName)

	// Check for errors.
	//
	if err != nil {
		return nil, err
	}

	// Return result.
	//
	return sqlconn, nil
}

// EstablishConnectionWithRetry - waits until the connection will be established.
func EstablishConnectionWithRetry(conn *sql.DB) {

	// Create a new exponential backoff.
	//
	b := backoff.NewExponentialBackOff()

	// We wait forever until the connection will be established.
	// In practice container orchestrators will kill the container if it takes too long.
	//
	b.MaxElapsedTime = 0

	// Establish connection.
	//
	_ = backoff.Retry(func() error {
		log.Println("Connecting to a database ...")

		// Ping verifies a connection to the database is still alive,
		// establishing a connection if necessary.
		//
		if errPing := conn.Ping(); errPing != nil {
			log.Println("Ping failed", errPing)
			return errPing
		}
		return nil
	}, b)
}

// RunWriteQuery - runs write query.
func RunWriteQuery(db *sql.DB, query string, args ...interface{}) (interface{}, error) {
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})

	if err != nil {
		return nil, err
	}
	defer func() {
		_ = tx.Rollback() // The rollback will be ignored if the tx has been committed later in the function.
	}()

	stmt, err := tx.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close() // Prepared statements take up server resources and should be closed after use.

	queryResult, err := stmt.Exec(args...)

	if err != nil {
		return nil, err
	}
	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return queryResult, err
}

// RunReadQuery - runs read query.
func RunReadQuery(db *sql.DB, query string, args []interface{}, f func(*sql.Rows) error) error {
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	queryResult, err := db.QueryContext(ctx, query, args...)
	defer func() {
		_ = queryResult.Close()
	}()
	if err != nil {
		return err
	}

	for queryResult.Next() {
		err = f(queryResult)
		if err != nil {
			return err
		}
	}
	return nil
}
