package sqldb

import (
	"strings"

	"github.com/dochq/postgresql.data-migration/internal/domain"
)

type parser struct {
}

// NewQueryParser creates a new query parser.
func NewQueryParser() domain.QueryParser {
	return &parser{}
}

func (p *parser) ParseQueries(lines []string) (queries []string, err error) {
	query := ""
	for _, line := range lines {
		if strings.HasPrefix(line, "//") {
			continue
		}
		query += line + "\n"
		if strings.HasSuffix(line, ";") {
			queries = append(queries, query)
			query = ""
		}
	}
	return queries, nil
}
