module github.com/dochq/postgresql.data-migration

go 1.17

require (
	github.com/cenk/backoff v2.2.1+incompatible
	github.com/lib/pq v1.10.5
)
